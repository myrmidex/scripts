#!/bin/bash

printf "\n"
printf "Installing Java..."
printf "\n"
sudo apt update
sudo apt install -y openjdk-17-jre

printf "\n"
printf "Installing Jenkins..."
printf "\n"
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install -y jenkins

sudo ufw allow 8080