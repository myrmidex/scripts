#!/bin/bash

# PACMAN
pacman -Syu
sudo pacman -S --needed git signal-desktop element-desktop chromium thunderbird

# GIT CONFIG
echo "Setting git config..."
git config --global alias.co checkout
git config --global alias.cm "commit -m"
git config --global alias.st status

# YAY
echo "Installing yay packages..."
yay -S --answerclean All --answerdiff All --needed \
	jetbrains-toolbox \
	mullvad-vpn-bin \
	revolt-desktop-git \
	spotify
